# README #

A basic R course oriented toward data analysis.

### If you just want to use it ###

Read Descriptive.pdf

### If you want to contribute ###
This course is released under the LGPL. Contributions are welcome:  

* typos 
* needs for clarifications
* formatting
* Important things I would have missed 

If you want to contribute, just fork and make a pull request or add an "issue". 