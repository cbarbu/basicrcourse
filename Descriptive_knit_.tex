% To compile this doc:
% In R: library("knitr"); knit("Descriptive_knit_.tex")
% Then on the command line : pdflatex Descriptive.tex 
% will generate Descriptive.pdf 
\documentclass{article}
\usepackage[ascii]{inputenc}
\usepackage[LGR,T1]{fontenc}
\usepackage[greek,english]{babel}
\usepackage{amsmath}
\usepackage{amssymb,amsfonts,textcomp}
\usepackage{dsfont}
\usepackage{array}
\usepackage{hhline}
\usepackage{graphicx}
\providecommand{\e}[1]{\ensuremath{\times 10^{#1}}}
\bibliographystyle{plos2009}
% \usepackage{supertabular}
\usepackage{booktabs}
\usepackage{changepage}
\usepackage[hidelinks]{hyperref}
% pour insertion ou non des commentaires
\usepackage{ifthen}
\newboolean{InsCom}		
\newcommand{\ComProf}[1]
{
\ifthenelse{\boolean{InsCom}}
{
\begin{quote}
  \itshape #1
\end{quote}
}
{}
}
\setboolean{InsCom}{true}      % true = inclure les commentaires ; false =ne pas les inclure.

\title{
Basics of R: import, visualize, analyse
}

\author{Corentin M.Barbu}

%% for knitr
%% for inline R code: if the inline code is not correctly parsed, you will see a message
\newcommand{\rinline}[1]{SOMETHING WRONG WITH knitr}
%% begin.rcode setup, include=FALSE
% require(xtable)
% opts_chunk$set(fig.path='figure/latex-', cache.path='cache/latex-',echo=FALSE)
%% end.rcode

\date{}

\usepackage[figurename=Figure,tablename=Table]{caption}
% \renewcommand{\thetable}{S1.\arabic{table}}
% \renewcommand{\thefigure}{S1.\arabic{figure}}

%vertically centered multi line cell default horizontally centered but l/r are accepted
\newcommand{\mlc}[2][c]	
{
\begin{tabular}[c]{@{}#1@{}} #2 \end{tabular}
}

\begin{document}
\maketitle

\tableofcontents

\vspace{0.5cm}

\section{Introduction}
  \subsection{Who am I}
   \subsection{About R}

  The strengths of R: 
  \begin{enumerate}
    \item Complete: almost everything invented in statistics is implemented in R through packages, managed by the CRAN. Just google: \begin{verbatim}CRAN whatever functionality I want\end{verbatim} 
    \item Flexibility: if it is not invented yet you can extend R easily 
    \item Documentation: good documentation with \textbf{examples} for all implemented functionality. On the R command line just type:
      \begin{verbatim}
      ?myfunction
      \end{verbatim}
    \item Easy scripting: once you have written a bunch of command that does a nice analysis, you can keep these commands in a text file and apply that again whenever you want. 
  \end{enumerate}

  The weaknesses of R: 
  \begin{enumerate}
    \item If you want to do push button analysis, it's not the right soft.
    \item Forces you to read a minimum of documentation to use something (and that may prevent you to use a hammer on a screw).
    \item The diversity of authors sometimes generate inconsistencies in terminology $\Rightarrow$ read the doc
  \end{enumerate}

  \subsection{Prep for this tutorial}
    \subsubsection{Install R}
      I very strongly recommend to beginners to use \href{http://www.rstudio.com/}{Rstudio}. 
    \subsubsection{Text editor}
      You can save R commands in any \em{text} file. Rstudio provides a nice text editor allowing to have many great interactions with the R console (the command line). If for some reason you do not use Rstudio, you can use any text editor (Vim, Emacs, nano etc.) but be sure to \em{never} use Microsoft word or any similar software as it will very likely mess up your code sooner or later. 
    \subsubsection{Working directory}
      A working directory is a folder where R is ``running'', where it will look for data etc. To tell R where is your working directory
    \begin{itemize}
      \item In Rstudio: on the right bottom corner window > files > select the folder and ``more'' > set as working directory
      \item in Rgui: File > Change Dir
      \item Commande line: \verb+setwd("path")+ and you can use the tab key to write the path progressively
    \end{itemize}

  Then when downloading the data (section \ref{sec:Import}) you should put the data in this same folder.

\section{Basic operations: R as a super calculator}
%% import the data
%% begin.rcode basic-operations, echo=TRUE
% # sharp sign makes the line to be a comment 
% # comment = something not read by R but usefull for people to understand
% 2+2
% # variables: results of operations can be given arbitrary names
% arbitrary <- 2*7
% # that can be displayed just writting its name
% arbitrary
% # and be re-used (try enter arb and then tab key)
% arbitrary+1
% # you can organize things in vectors 
% myvector <- c(1,3,5)
% myvector
% # and we can specify a range with ":"
% myvector2<- c(1,4:6)
% myvector2
% # of length:
% length(myvector2)
% # interestingly we can find which items obey a condition
% which(myvector2 > 5)
% # or even multiple conditions
% which(myvector2 > 5 | myvector2 == 1) # | for OR
% which(myvector2 > 5 & myvector2 == 1) # & for AND
% # meaning the vector is empty: no line obey our second set of conditions
%% end.rcode

Please use the \verb+tab+ key as much as possible to avoid errors.

\section{Import data}\label{sec:Import}
  \subsection{Basic import}
We will use the following dataset:

\url{https://github.com/cbarbu/spatcontrol/blob/PLoScompStreets/JitteredDataPaucarpata.csv}

That correspond to a simulated data set of infestation by insects vectors of Chagas disease in households of Arequipa, Peru\cite{Barbu2013}.

%% import the data
%% begin.rcode my-cache, cache=TRUE, echo=TRUE
% dat<-read.csv("JitteredDataPaucarpata.csv")
%% end.rcode

It is always a good idea to check that it was imported correctly:
%% begin.rcode ex-head, cache=TRUE, echo=TRUE
% # the dimension of the table imported:
% dim(dat)
% # and how look the first lines:
% head(dat)
% # or last lines
% tail(dat)
% # and get a summary of what I've got
% summary(dat)
% # possibly check the structure
% str(dat)
%% end.rcode

Where:
\begin{description}
  \item[X] latitude in UTM (meters) 
  \item[Y] longitude in UTM (meters) 
  \item[observed] household inspected (1) or not (0)
  \item[GroupNum] city block number
  \item[CU] presence of guinea pigs (Cuy in spanish)
  \item[PE] presence of dogs (Perro in spanish)
  \item[oanimal] presence of other animals
  \item[I.NO] inside of the houses has plastered walls 
  \item[P.NO] outside of the house is plastered
  \item[IdObserver] Id of the inspector
  \item[positive] household infested 
  \item[bgen,ygen,wgen,vgen,cgen,ugen,fitSet] let's not detail that here
\end{description}

You may want to import things that are not as regular as a basic csv. For example, your data may be a simple table, delimited by spaces and without headers, something like:
\begin{verbatim}
1 2
3 4
5 6 
\end{verbatim}

Just check the help:
\begin{verbatim}
help(read.csv)
\end{verbatim}
or
\begin{verbatim}
?read.csv
\end{verbatim}

And you can see that the second option is ``header'' allowing to consider the first line as headers or not. The third option being ``sep'' allowing to specify the delimiter.

You can see that the help on an R function is very detailed, so first reflex in case of troubles: look at the help.

\subsection{Changing data format}
    \subsubsection{Factors}
      Factors are much used by default by R in dataframes. A factor has levels and indexes. 
      The levels corresponds to the different values your parameter can take, ordered and indexed, then the values in your column are replaced by the index of the corresponding level. 
      csv are by default imported in dataframes with factors for the character strings. 
      To prevent it, just use \begin{verbatim}stringsAsFactors=FALSE\end{verbatim} in {\verb+read.csv+}.

      Factors are useful to save memory space and to some extend speed up calculations. 

      You can move to and from factors any kind of values but careful when converting back factors back to numbers, you need to always convert first to character and then to numeric:

%% begin.rcode factorNum, echo=TRUE
% dat$GroupNumFact <- as.factor(dat$GroupNum) 
% dat$GroupNumBackNum <- as.numeric(dat$GroupNumFact)
% dat$GroupNumBackNumOK <- as.numeric(as.character(dat$GroupNumFact))
% head(dat[,c("GroupNum","GroupNumFact","GroupNumBackNum","GroupNumBackNumOK")])
%% end.rcode
GroupNumBackNum is messed up: it represents as integer the index of the value in the levels dictionary, not the value itself.

    \subsubsection{Dates}
      Dates are usually imported initially as character strings. You can make them dates using \verb+as.Date+. For example this format ``31/12/2013'' can be ``read'' using:
%% begin.rcode basicDates, echo=TRUE
% startDate <- as.Date("31/12/2013",format="%d/%m/%Y") 
% str(startDate)
% endDate <- as.Date("30-11-2014",format="%d-%m-%Y") 
% str(endDate)
%% end.rcode
      Dates and times can then be converted to numbers to ease calculations using \verb+as.POSIXct+. For example to create a column of random dates in our data frame:
%% begin.rcode randomDates, echo=TRUE
% GenRandomDates <- function(N, st, et) {
%    st <- as.POSIXct(st)
%    et <- as.POSIXct(et)
%    dt <- as.numeric(difftime(et,st,unit="sec"))
%    ev <- sort(runif(N, 0, dt))
%    rt <- st + ev
%    return(as.Date(rt))
% }
% dat$dates <- GenRandomDates(dim(dat)[1],startDate,endDate)
% 
% dat$GroupNumBackNumOK <- as.numeric(as.character(dat$GroupNumFact))
% head(dat[,c("GroupNum","GroupNumFact","GroupNumBackNum","GroupNumBackNumOK")])
%% end.rcode

      \section{Visualize}
  We've already visualized some summary about the data. Here is some more, what about the relationship between having a dog and having a guinea pig:
%% begin.rcode table, echo=TRUE
% table(dat$CU,dat$PE) # the first item specify the line(s) the second the columns
%% end.rcode

  Let's plot the corresponding map:
\begin{figure}[htpb]
  \begin{center}
%% begin.rcode cairo-simplePlot, dev='cairo_pdf', fig.width=6, fig.height=6, out.width='.7\\textwidth', echo=TRUE, cache=TRUE
% a<-plot(dat$X,dat$Y,asp=1)
%% end.rcode

  \end{center}
  \caption{Sampled households\newline
    {\footnotesize
    Each circle is a household, 
    }
  }
  \label{fig:simplePlot}
\end{figure}

And if you wonder why I use \verb+asp=1+ the help for plot will inform you that it forces the x/y ratio to be 1.
%
Note that \verb+$+ allows us to refer to a column in the dataframe.

To refer to a specific line we use \verb+[]+. For example:

%% begin.rcode subset, echo=TRUE
% dat[1,] # the first item specify the line(s) the second the columns
%% end.rcode

Note that empty means \emph{all}.

We can also do complex subsets using vectors of lines and columns:

%% begin.rcode subset-medium, echo=TRUE
% dat[c(1:3),c("X","Y")] # the first item specify the line(s) the second the columns
%% end.rcode

More usefull we can select lines with specific characteristics
%% begin.rcode subset-complex, echo=TRUE
% observed <- dat[which(dat$observed == 1),] # the first item specify the line(s) the second the columns
% # and observed its dimension
% dim(observed)
%% end.rcode
We have created \verb+observed+ that has \rinline{dim(observed)[1]} lines and \rinline{dim(observed)[2]} columns, indicating that households that were observed.

Let's show the participation and infestation and discover \verb+with()+.
\begin{figure}[htpb]
  \begin{center}
%% begin.rcode cairo-detailPlot, dev='cairo_pdf', fig.width=6, fig.height=6, out.width='.99\\textwidth',cache=TRUE, echo=TRUE
% # this is a comment
% with(dat,plot(X,Y,asp=1,pch="."))
% with(observed,points(X,Y,asp=1,pch=19,col="blue"))
% with(dat[which(dat$positive==1),],points(X,Y,asp=1,pch=19,col="yellow",cex=0.5))
%% end.rcode
  \end{center}
  \caption{Sampled households\newline
    {\footnotesize
    Each dot is a household, 
    }
  }
  \label{fig:detailPlot}
\end{figure}

Then you can save as pdf for example your nice plot with \verb+dev.print()+:
%% begin.rcode savePlot, echo=TRUE
% dev.print(dev=pdf,"myNicePlot.pdf")
%% end.rcode
Rstudio also has a nice export button above the plot.

\section{Basic data handling}
  \subsection{Making a new column in the dataframe}
    Let's visualize a new class of interest: any animal. As this may be of use in general analysis we want to keep this information in the general table:
%% begin.rcode any-animal, echo=TRUE
% # add "anyAnimal" to dat
% dat$anyAnimal <- as.numeric(dat$oanimal == 1 | dat$PE == 1 | dat$CU == 1)
%% end.rcode

%% begin.rcode typepointplot, dev='cairo_pdf', fig.width=6,fig.height=3,out.width='.99\\textwidth',echo=TRUE
% # as we will need different point types let's list them all:
% plot(1:25,rep(1,25),pch=1:25)
%% end.rcode

%% begin.rcode otherAnimalPlot, dev='cairo_pdf', fig.width=12,fig.height=6,out.width='.99\\textwidth',echo=TRUE
% # par allows to redefine the layout of the plotting region
% par(mfrow=c(1,2)) # organize graphis in 1 row, 2 columns, fill by row
% # first all animals together
% with(dat, plot(X,Y,pch=".",main="Any animals",asp=1))
% with(dat[dat$anyAnimal == 1,], lines(X,Y,pch=3,col="red",type="p"))
% # then animals separatly
% with(dat, plot(X,Y,pch=".",main="All animals",asp=1))
% with(dat[dat$CU== 1,], lines(X,Y,pch=3,col="red",type="p"))
% with(dat[dat$PE== 1,], lines(X,Y,pch=4,col="green",type="p"))
% with(dat[dat$oanimal== 1,], lines(X,Y,pch=5,col="blue",type="p"))
% legend("topleft",c("Guinea Pigs","Dogs","Other animals"),col=c("red","green","blue"),pch=c(3,4,5))
%% end.rcode
This seems a little difficult to read. But you will fix it by yourself section \ref{sec:zoom}.

    \subsection{Stacking vectors and tables together}
    This may be useful if you generate data or to format a nice output
%% begin.rcode make-table, echo=TRUE
% # make some vectors
%  labels<-c("pretty","labels") 
%  values<-c(3,2)  
% # the make them columns of a same matrix 
%  output<-cbind(labels,values)
% # this can be saved as a csv to go in an article
%% end.rcode

\section{Example of statistical analyse: glm}
%% begin.rcode model-animal, echo=TRUE
% # let's look at the impact of different animals on the infestation
% model1<-glm(positive ~ CU+PE+oanimal,family=binomial(link="logit"),data=dat[which(dat$observed==1),])
% summary(model1)
% # what if we take all the animals together?
% model2<-glm(positive ~ anyAnimal,family=binomial(link="logit"),data=dat[which(dat$observed==1),])
% summary(model2)
%% end.rcode

You can actually predict for each point according to the best model, here the first and save it in your table
%% begin.rcode predict-animal, echo=TRUE
% dat[which(dat$observed==1),"predict"] <- predict(model1)
%% end.rcode

\section{Export data}
  You may want to export your data to another csv:
%% begin.rcode export-animal, echo=TRUE
%  write.csv(dat,file="data_with_predictions.csv")
%% end.rcode
  Alternatively you might want to save in R format (for example to include in an R package)
%% begin.rcode save-animal, echo=TRUE
%  save(dat,file="data_with_predictions.rda")
%% end.rcode

\section{Write functions}
  A function allows to group instructions. It has three characteristics:
  \begin{itemize}
    \item a name to identify it
    \item entries or arguments fed to the function
    \item outputs returned by the function
  \end{itemize}
  In practice the most sensible functions can have only a name
  \subsection{Basic structure}
    A function doing nothing: 
%% begin.rcode basicFunction, echo=TRUE
% DoNothing <- function(){}
%% end.rcode
    DoNothing is a function with no entries (nothing between parenthesis) and doing nothing (nothing between curly brackets). It can be called and \dots do nothing: 
%% begin.rcode basicFunctionUse, echo=TRUE
% DoNothing()
%% end.rcode
    \subsubsection{Functions without a name}
      In some cases it can be interesting to define a very short lived function, that hasn't a name, usually as an argument of an other function. For example with \texttt{aggregate()}, if you want to aggregate the lines in a table keeping only the last occurence:
%% begin.rcode basicNoName, echo=TRUE
% dat <- cbind(group=rep(1:3,3),id=1:9)
% aggregate(dat[,"id"],by=list(dat[,"group"]),
%           function(x){x[length(x)]})
%% end.rcode

  \subsection{Handling outputs}
    \subsubsection{Basic outputs}
      Now we can do something, like display ``Hello world'' and return 1:
%% begin.rcode basicFunctionOutput, echo=TRUE
% HelloWorld <-function(){
%     cat("Hello world !")
%     return(1)
% }
%% end.rcode
      And it works:
%% begin.rcode basicFunctionOutputUse, echo=TRUE
% HelloWorld()
%% end.rcode

      \subsubsection{Multiple outputs}
      To return multiple outputs you need to put them in a list: 
%% begin.rcode ComplexOutputs, echo=TRUE
% MakeMyComplexOutputs <- function(){
%     mat <- matrix(1:4,nrow=2)
%     return(list(a="a",b=123,c=mat))
% }
%% end.rcode
      And you can retrieve them like in any list using \verb+$+ or \verb+[[``item'']]+: 
%% begin.rcode ComplexOutputsUse, echo=TRUE
% out <- MakeMyComplexOutputs() 
% out$a
% out[["b"]]
% out[["c"]]
%% end.rcode
      
\subsubsection{Use \texttt{return()}!}
      Note that R has the evil functionality to return by default the last thing computed in the function. 
      Some will argue that it makes for shorter and ``hence'' easier to read code, so why is it evil? 
      You might very well in the future want to add something to your function and given that \verb+return+ is 
      not here to remind you that you should not put anything after it you could do things like that:
%% begin.rcode UseReturn, echo=TRUE
% MakeMyComplexOutputs1 <- function(){
%     mat <- matrix(1:4,nrow=2)
%     a<- 2
%     b<- mat/2
% }
% MakeMyComplexOutputs2 <- function(){
%     mat <- matrix(1:4,nrow=2)
%     a<- 2
%     b<- mat/2
%     cat(b[1,2])
% }
% out1 <- MakeMyComplexOutputs1()
% out2 <- MakeMyComplexOutputs2()
% out1
% out2
%% end.rcode
      There are enough occasions to create bugs without that.

      \subsubsection{Do not clutter your command line}
      You might want the function to return say a dataset without the having it displayed when you call the function. 
      Simply use \verb+invisible()+:
%% begin.rcode UseInvisible, echo=TRUE
% MakeMyComplexOutputs3 <- function(){
%     mat <- matrix(1:4,nrow=2)
%     a<- 2
%     b<- mat/2
%     return(invisible(b))
% }
% MakeMyComplexOutputs3()
%% end.rcode
      Note that nothing is displayed, not even \texttt{NULL}.

  \subsection{Handling entries}
    \subsubsection{Simple entries}
      Entries can be specified simply based on their order:
%% begin.rcode UseEntries, echo=TRUE
% LinearOp <- function(a,b){
%     return(a+2*b)
% }
%LinearOp(1,2)
%% end.rcode
      But also using names, that of course take precedence over the order.
%% begin.rcode UseNamedEntries, echo=TRUE
% LinearOp(b=1,a=2)
% LinearOp(a=1,b=2)
%% end.rcode
      Using systematically names can be a good idea in complex codes, specially when reliying on default values and using the special \dots argument. 
    \subsubsection{Setting default values}
      Default values for arguments can be set very easily: 
%% begin.rcode UseDefaultValues, echo=TRUE
% LinearOp <- function(a=3,b=4){
%     return(a+2*b)
% }
% LinearOp()
% LinearOp(b=10)
% LinearOp(1)
%% end.rcode

    \subsubsection{The special \dots argument}
      If you call an other function from your function, you might want to pass an undefined 
      list of arguments to this function:
%% begin.rcode UseDefaultTripleDots, echo=TRUE
% AlwaysAddMyHouse <- function(X,Y,xhouse=0.5,yhouse=0.5,...){
%     plot(X,Y,...)
%     points(xhouse,yhouse,col="blue",pch="x",cex=3)
% }
% AlwaysAddMyHouse(X=rnorm(10),Y=rnorm(10),main="My city")
%% end.rcode

  \subsection{Recursivity}
    Many algorithms are elegantly resolved using recursivity: the function calls itself. This is not an issue in R. 
    The standard example is to compute a factorial : the product of $n*(n-1)*\dots*3*2*1$:
%% begin.rcode RecursiveExample, echo=TRUE
% Factorial <-function(n){
%   if(n>2){
%       prod <- n*Factorial(n-1)
%   }else{
%       prod <- n
%   }
%   return(prod)
% }
% Factorial(3)
% Factorial(5)
%% end.rcode
 
\section{How to get further?}
\subsection{Typical workflow for new analysis}
  You will most often not know (or have forgotten) what function to use so:
  \begin{itemize}
    \item google \verb+CRAN the analysis I want to perform+ $\rightarrow$ package manual $\rightarrow$ read the manual, check that it does what I want and identify  $\rightarrow$ install the package 
    \item I find a function in google $\rightarrow$ if it is in the \verb+base+ package, just check the help \verb+?newfunction+
    \item If it is not I can check if I already installed this package
      \begin{verbatim}
      library(superpackage)
      \end{verbatim}

    \item If the package is not found I install it
      \begin{verbatim}
      install.packages(superpackage)
      \end{verbatim}
    \item And then load it:
      \begin{verbatim}
      library(superpackage)
      \end{verbatim}
      This last step must be repeated for each new session where I want to use this function
    \item Then I can look at the help and go ahead
      \begin{verbatim}
      ?newfunction
      \end{verbatim}
  \end{itemize}

  \subsection{Let's do it}\label{sec:zoom}
    Some of these graph were a bit messy, we need to zoom in on these graphs.
    look for \verb+CRAN zoom+ in google, install and use the package.

\section{About this document}
  \subsection{How to get it}
  You can get it googling:

  \verb+ cbarbu bitbucket+

  Then choose cbarbu, basicRcourse, Source, and download ``Descriptive.pdf''

  \subsection{Remarks? Corrections?}
    Please fill free to clone the repository and make a pull request. If you don't know what it means, just write me an email at corentin.barbu at gmail dot com. 

\bibliography{chagas} 
\end{document}
